<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Wink\WinkPost;
use Wink\WinkTag;


class PostController extends Controller
{
	public function index(){
	    $posts = WinkPost::with('tags','author')
	        ->live()
	        ->orderBy('publish_date', 'DESC')
	        ->simplePaginate(12);

	    return view('blog.index', [
	        'posts' => $posts
	    ]);
	}

	public function post($post_id){
	    $posts = WinkPost::with('tags','author')
	        ->where('wink_posts.id',$post_id)
	        ->first();

	    $allTags = WinkTag::get();

	    return view('blog.post', [
	        'posts' => $posts,
	        'allTags' => $allTags
	    ]);
	}

	public function tagSearch($tag_id){
	    $posts = WinkPost::with('tags','author')
	        ->whereHas('tags', function ($query) use ($tag_id) {
				$query->where('id', $tag_id);
			})
	        ->get();
	    $allTags = WinkTag::get();

	    return view('blog.tags', [
	        'posts' => $posts,
	        'allTags' => $allTags
	    ]);
	}
}
