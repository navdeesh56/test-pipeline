@extends('include.index')

@section('content')
	
    <div class="container">
      <div class="row">
        <!-- Latest Posts -->
        <main class="posts-listing col-lg-8"> 
          <div class="container">
            <div class="row">
              <!-- post -->
              @foreach($posts as $post)
              <div class="post col-xl-6">
                <div class="post-thumbnail"><a href="{{ url('post/'.$post->id) }}"><img src="{{$post->featured_image}}" alt="..." class="img-fluid"></a></div>
                <div class="post-details">
                  <div class="post-meta d-flex justify-content-between">
                  </div><a href="{{ url('post/'.$post->id) }}">
                    <h3 class="h4">{{$post->title}}</h3></a>
                  <p class="text-muted">{{ substr(strip_tags($post->body),0,150) }}...</p>
                  <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                      <div class="avatar"><img src="{{$post->author->avatar}}" alt="..." class="img-fluid"></div>
                      <div class="title"><span>{{$post->author->name}}</span></div></a>
                    <div class="date"><i class="icon-clock"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$post->publish_date)->format('M Y')}}</div>
                  </footer>
                </div>
              </div>
              @endforeach
            </div>

            <!-- Pagination -->
            <nav aria-label="Page navigation example">
              <ul class="pagination pagination-template d-flex justify-content-center">
                <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-left"></i></a></li>
                <li class="page-item"><a href="#" class="page-link active">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link"> <i class="fa fa-angle-right"></i></a></li>
              </ul>
            </nav>
          </div>
        </main>
        <aside class="col-lg-4">
          <!-- Widget [Search Bar Widget]-->
          <div class="widget search">
            <header>
              <h3 class="h6">Search the blog</h3>
            </header>
            <form action="#" class="search-form">
              <div class="form-group">
                <input type="search" placeholder="What are you looking for?">
                <button type="submit" class="submit"><i class="icon-search"></i></button>
              </div>
            </form>
          </div>
          <div class="widget tags">       
            <header>
              <h3 class="h6">Tags</h3>
            </header>
            <ul class="list-inline">
              @foreach($allTags as $tag)
              <li class="list-inline-item"><a href="{{url('/tag/'.$tag->id)}}" class="tag">#{{$tag->name}}</a></li>
              @endforeach
            </ul>
          </div>
        </aside>
      </div>
    </div>
    
@endsection