@extends('include.index')

@section('content')
<body>
	@foreach($posts as $post)
		<h1>
			{{$post->slug}}
		</h1>
			updated on: {{$post->updated_at}}
		<div>
			{!!html_entity_decode($post->body)!!}
		</div>
	@endforeach
</body>
@endsection
