@extends('include.index')

@section('content')

    <section style="background: url(img/hero.jpg); background-size: cover; background-position: center center" class="hero">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h1>Test Blog</h1>
          </div>
        </div>
      </div>
    </section>
    <section class="featured-posts no-padding-top">
      <div class="container">

      	@foreach($posts as $post)
      	<!-- {{print_r($post)}} -->
        <div class="row d-flex align-items-stretch">
          <div class="text col-lg-7">
            <div class="text-inner d-flex align-items-center">
              <div class="content">
                <header class="post-header">
                  <!-- <div class="category">Business</a><a href="#">Technology</a></div><a href="post.html"> -->
                    <a href="{{ url('post/'.$post->id) }}"><h2 class="h4">{{$post->title}}</h2></a>
                </header>
                <p>{{ substr(strip_tags($post->body),0,150) }}...</p>
                <footer class="post-footer d-flex align-items-center"><a href="#" class="author d-flex align-items-center flex-wrap">
                    <div class="avatar"><img src="{{$post->author->avatar}}" alt="..." class="img-fluid"></div>
                    <div class="title"><span>{{$post->author->name}}</span></div></a>
                  <div class="date"><i class="icon-clock"></i>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$post->publish_date)->format('M Y')}}</div>
                </footer>
              </div>
            </div>
          </div>
          <div class="image col-lg-5"><img src="{{$post->featured_image}}" alt="..."></div>
        </div>
        @endforeach
      </div>
    </section>
    <!-- Divider Section-->
    <!-- <section style="background: url(img/divider-bg.jpg); background-size: cover; background-position: center bottom" class="divider">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</h2><a href="#" class="hero-link">View More</a>
          </div>
        </div>
      </div>
    </section> -->
@endsection